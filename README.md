# Letz - Code Challenge

## Objetivo

O objetivo desse desafio é avaliar suas habilidades em áreas relevantes ao desenvolvimento mobile na Letz. 

Isso inclui:

- Habilidade de utilizar React-Native/Javascript ao interagir com APIs.
- Conhecimentos em criar e implementar uma interface básica, utilizando boas práticas para oferecer uma boa experiência aos usuários.
- Experiência com Javascript para criar um codigo limpo, legível e performático.

*Palavras-chave*
Funcionalidade | Formatação | Estrutura do Projeto | Escalabilidade | Manutenção | Boas práticas de mercado

## Detalhes

O projeto deverá utilizar a PokéAPI - [The RESTful Pokémon API](https://pokeapi.co/) para desenvolver uma aplicação onde se possa listar, buscar e favoritar pokemons.

O uso de bibliotecas externas (Axios, Redux, React Navigation etc) é liberado.
Apenas faça bom uso delas, a dica é não esquecer que esse espaço é para mostrar *o que você sabe fazer*.

Não vamos avaliar o design, mas sim a experiência do usuário em todas as suas interações com o app.

## O que preciso fazer?

- [ ] Utilizar [PokéAPI](https://pokeapi.co/) 
- [ ] Tela de listagem de pokemons
- [ ] Mecanismo de busca por nome dos pokemons
- [ ] Tela onde possa listar os pokemons favoritos (não é necessário persistir os dados).
- [ ] React-Native/Javascript como tecnologia.
- [ ] Testes unitários

## O que vamos avaliar?

- Uso correto do gerenciamento de state
- Boa navegação entre as funcionalidades
- Componentização
- Boas práticas nas chamadas de API e tratamento de dados
- Separação de responsabilidades das lógicas das regras de negócio e interface

## Pontos extras

- Typescript
- Filtros, ordenação e paginação são sempre bem-vindos.
- Um resumo de como foi feito o desenvolvimento, bem como um plano para as implementações que gostaria de ter feito.
